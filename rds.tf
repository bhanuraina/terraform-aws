resource "aws_db_instance" "test"{
  
    allocated_storage                     = 20
    auto_minor_version_upgrade            = true
    availability_zone                     = var.availability_zones[1]
    backup_retention_period               = 7
    backup_window                         = "05:02-05:32"
    ca_cert_identifier                    = "rds-ca-2019"
    copy_tags_to_snapshot                 = true
    customer_owned_ip_enabled             = false
    
    db_subnet_group_name                  = aws_db_subnet_group.this.id
    delete_automated_backups              = true
    deletion_protection                   = false
    enabled_cloudwatch_logs_exports       = []
    engine                                = "mysql"
    engine_version                        = "8.0.28"
   
    
    iam_database_authentication_enabled   = false
    identifier                            = "${local.Environment}-spring"
    instance_class                        = "db.t3.micro"
    iops                                  = 0
    kms_key_id                            = aws_kms_key.this.arn
    
    license_model                         = "general-public-license"
    maintenance_window                    = "tue:08:51-tue:09:21"
    max_allocated_storage                 = 1000
    monitoring_interval                   = 0
    multi_az                              = false
    db_name                                  = "springdata"
    option_group_name                     = "default:mysql-8-0"
    parameter_group_name                  = "default.mysql8.0"
    performance_insights_enabled          = false
    performance_insights_retention_period = 0
    port                                  = 3306
    publicly_accessible                   = true
    
    
    security_group_names                  = []
    skip_final_snapshot                   = true
    
    storage_encrypted                     = true
    storage_type                          = "gp2"
    tags                                  = {
        Name        = "${local.Application}-rds"
        Environment = local.Environment
    }
    tags_all                              = {}
    username                              = "admin"
    password                              = var.mysql_password
    vpc_security_group_ids                = [
        aws_security_group.db_security_group.id,
    ]

    timeouts {}
}

resource "aws_security_group" "db_security_group" {
  vpc_id = aws_vpc.Main.id

  ingress {
    from_port        = 3306
    to_port          = 3306
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = {
    Name        = "${local.Application}-sg"
    Environment = local.Environment
  }
}

resource "aws_db_subnet_group" "this" {
  name       = "${local.Environment}-dbgroup"
  subnet_ids = [aws_subnet.privatesubnets.id]

  tags = {
        Name        = "${local.Application}-dbgroup"
        Environment = local.Environment
  }
}
resource "aws_kms_key" "this" {
  description             = var.kms_key_description
  deletion_window_in_days = var.kms_key_deletion_window_in_days
  enable_key_rotation     = var.kms_key_enable_key_rotation

  tags  = {
     Name        = "${local.Application}-s3-kms"
     Environment = local.Environment
  }
}
