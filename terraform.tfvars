main_vpc_cidr = "10.0.0.0/16"
public_subnets     = ["10.0.0.0/24", "10.0.1.0/24"]
private_subnets = "10.0.128.0/24"
aws_region = "us-east-2"
availability_zones = ["us-east-2a", "us-east-2b","us-east-2c"]
