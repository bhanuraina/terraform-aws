variable "main_vpc_cidr" {}
variable "public_subnets" {}
variable "private_subnets" {}
variable "aws_region" {}
variable "availability_zones" {
  description = "List of availability zones"
}
variable "mysql_password"{
  default = ""
}

variable "kms_key_description" {
  description = "The description of the key as viewed in AWS console."
  type        = string
  default     = "The key used to encrypt the remote state bucket."
}

variable "kms_key_deletion_window_in_days" {
  description = "Duration in days after which the key is deleted after destruction of the resource, must be between 7 and 30 days."
  type        = number
  default     = 30
}

variable "kms_key_enable_key_rotation" {
  description = "Specifies whether key rotation is enabled."
  type        = bool
  default     = true
}

locals {

    Environment = "dev"
    Application = "Spring"
    Name = "spring-ecs"
}
